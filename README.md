# Foreign Agents Registration Act (FARA) Data

This is a scrapy project to scrape data from https://efile.fara.gov/pls/apex/f?p=185:130:0::NO:RP,130:P130_DATERANGE:N

To run
 - scrapy crawl fara
 
 Output would be found in data.json file in this format
 ```
{
  ​​"url"​​: "https://efile.fara.gov/pls/apex/f?p=171:200:::NO:RP,200:P200_REG_NUMBER,P200_DOC_TYPE,P200_CO UNTRY:2310,Exhibit%20AB,BAHAMAS"​,
  ​​"country"​​: ​​"BAHAMAS"​,
  ​​"state"​​: ​​null​,
  ​​"reg_num"​​: ​​"2310"​,
  "address"​: ​​"Nassau"​,
  ​​"foreign_principal"​​: ​​"Bahamas Ministry of Tourism"​,
  ​​"date"​​: ISODate​(​"1972-01-27T00:00:00Z"​),
  ​​"registrant"​​: ​​"Bahamas Tourist Office"​,
  ​​"exhibit_url"​​: "http://www.fara.gov/docs/2310-Exhibit-AB-19720101-DBBMB702.pdf"​
}
 ```
To run test
 - python -m unittest faraproject.tests.tests

