import unittest
import json
import os

from faraproject.spiders.fara import FaraSpider
from faraproject.items import FaraprojectItem
from faraproject.tests.responses import fake_response_from_file

class TestFaraSpider(unittest.TestCase):

    def setUp(self):
        self.spider = FaraSpider()
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "responses/item_output.json")
        self.exhibit_url_link_files = [
            'exhibit_001.html'
        ]
        with open(path, encoding='utf-8') as f:
            item_outputs = json.loads(f.read())
            self.item_output = {}
            for count, value in enumerate(item_outputs):
                self.item_output[count] = value


    def test_parse(self):

        results = self.spider.parse(fake_response_from_file('fara.html'))
        count = 0
        for items in results:
            single_item_reg_list = items.meta.get('item', [])
            for item in single_item_reg_list:
                current_output_item = self.item_output[count]
                self.assertEqual(item['url'], current_output_item['url'])
                self.assertEqual(item['country'], current_output_item['country'])
                self.assertEqual(item['state'], current_output_item['state'])
                self.assertEqual(item['reg_num'], current_output_item['reg_num'])
                self.assertEqual(item['address'], current_output_item['address'])
                self.assertEqual(item['foreign_principal'], current_output_item['foreign_principal'])
                self.assertEqual(item['foreign_principal'], current_output_item['foreign_principal'])
                self.assertEqual(item['date'], current_output_item['date'])
                self.assertEqual(item['registrant'], current_output_item['registrant'])

                count += 1
            
    def test_exhibit_url(self):
        for count, single_exhibit_file in enumerate(self.exhibit_url_link_files):
            exhibit_result_url = self.spider.get_exhibit_url_from_response(fake_response_from_file(f"exhibit_files/{single_exhibit_file}"))
            self.assertEqual(self.item_output[count]['exhibit_url'], exhibit_result_url)
            print(self.item_output[count]['country'])

if __name__ == '__main__':
    unittest.main()
