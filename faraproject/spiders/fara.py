# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime

from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from faraproject.items import FaraprojectItem


class FaraSpider(scrapy.Spider):
    name = 'fara'
    allowed_domains = ['fara.gov']
    start_urls = ['https://efile.fara.gov/pls/apex/f?p=185:130:0::NO:RP,130:P130_DATERANGE:N']
    

    def remove_character_from_string(self, dirty_string):
        return dirty_string.replace('\xa0', ' ').strip() if dirty_string else None
        
    
    def parse(self, response):
    
        fara_datas = response.css('.apexir_WORKSHEET_DATA > tr')
        current_country = None
        exhibit_url_item_dict = {}
        for fara_data in fara_datas:
            item = FaraprojectItem()
            if fara_data.css('.apexir_REPEAT_HEADING > span::text').extract_first():
                current_country = fara_data.css('.apexir_REPEAT_HEADING > span::text').extract_first()
                continue
            if fara_data.css('th').extract():
                continue
            
            url = response.urljoin(fara_data.css('td > a::attr(href)').extract_first())
            
            state = self.remove_character_from_string(fara_data.xpath('td[contains(@headers, "STATE")]/text()').extract_first())
            reg_num = self.remove_character_from_string(fara_data.xpath('td[contains(@headers, "REG_NUMBER")]/text()').extract_first())
            address = self.remove_character_from_string(fara_data.xpath('td[contains(@headers, "ADDRESS_1")]/text()').extract_first())
            foreign_principal = self.remove_character_from_string(fara_data.xpath('td[contains(@headers, "FP_NAME")]/text()').extract_first())
            date = self.remove_character_from_string(fara_data.xpath('td[contains(@headers, "REG_DATE")]/text()').extract_first())
            registrant = self.remove_character_from_string(fara_data.xpath('td[contains(@headers, "REGISTRANT_NAME")]/text()').extract_first())
            

            item['url'] = url
            item['country'] = current_country
            item['state'] = state
            item['reg_num'] = reg_num
            item['address'] = address
            item['foreign_principal'] = foreign_principal
            item['date'] = datetime.strptime(date, "%m/%d/%Y").isoformat()
            item['registrant'] = registrant
        
            link_to_exhibit_url = response.urljoin(fara_data.xpath('td[contains(@headers, "LINK")]/a/@href').extract_first())
    
            if exhibit_url_item_dict.get(link_to_exhibit_url):
                exhibit_url_item_dict[link_to_exhibit_url].append(item)
            else:
                exhibit_url_item_dict[link_to_exhibit_url] = [item]


        for exhibit_url_link, item in exhibit_url_item_dict.items():
            request_exhibit_url = scrapy.Request(exhibit_url_link, callback=self.get_exhibit_url, dont_filter=True)
            request_exhibit_url.meta['item'] = item
            yield request_exhibit_url                
            
        has_next_page = response.xpath('//img[@title="Next"]').extract_first()

        if has_next_page is not None:
            next_page = "https://efile.fara.gov/pls/apex/wwv_flow.show"
            if 'params' not in response.meta:
                params = {
                        'p_request' : 'APXWGT',
                        'p_instance' : response.xpath('//input[@id="pInstance"]/@value').extract_first(),
                        'p_flow_id' : response.xpath('//input[@id="pFlowId"]/@value').extract_first(),
                        'p_flow_step_id' : response.xpath('//input[@id="pFlowStepId"]/@value').extract_first(),
                        'p_widget_num_return' : '15',
                        'p_widget_name' : 'worksheet',
                        'p_widget_mod' : 'ACTION',
                        'p_widget_action' : 'PAGE',
                        'x01': response.xpath('//input[@id="apexir_WORKSHEET_ID"]/@value').extract_first(),
                        'x02': response.xpath('//input[@id="apexir_REPORT_ID"]/@value').extract_first()
                    }
            else:
                params = response.meta['params']

            pagination_span = response.xpath('(//td[@class="pagination"]/span/a/@href)[last()]').extract_first()
            p_widget_action_mod = pagination_span[pagination_span.find("('") + 2 : pagination_span.find("')")]
            params['p_widget_action_mod'] = p_widget_action_mod

            req = scrapy.FormRequest(next_page, method="POST", formdata=params, callback=self.parse)
            req.meta['params'] = params
            
            yield req

    def get_exhibit_url_from_response(self, response):
        return response.xpath('(//td[@headers="DOCLINK"])[1]//@href').extract_first()

    def get_exhibit_url(self, response):
        exhibit_url = self.get_exhibit_url_from_response(response)
        item = response.meta['item']
        for single_item in item:
            single_item['exhibit_url'] = exhibit_url
            yield single_item
